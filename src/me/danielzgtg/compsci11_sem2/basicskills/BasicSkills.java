package me.danielzgtg.compsci11_sem2.basicskills;

import java.util.Arrays;

public final class BasicSkills {

	public static final boolean evenlyDivisibleBy3(final int check) {
		return check % 3 == 0;
	}

	public static final Number average1(final int num1, final int num2) {
		final int sign1 = Integer.signum(num1);

		if (sign1 == 0) {
			return num2 % 2 == 0 ? num2 / 2 : num2 * 0.5;
		}

		final int sign2 = Integer.signum(num2);

		if (sign2 == 0) {
			return num1 % 2 == 0 ? num1 / 2 : num1 * 0.5;
		}

		final boolean differentSign = sign1 != sign2;

		if (differentSign) {
			final int together = num1 + num2;

			return together % 2 == 0 ? together / 2 : together * 0.5;
		} else {
			final boolean even1 = num1 % 2 == 0;

			if (even1 ^ (num2 % 2 == 0)) { // One is even
				return (num1 / 2) + (num2 / 2) + 0.5 * sign1;
			} else if (even1) { // Both are even
				return (num1 / 2) + (num2 / 2);
			} else { // Both are odd
				return (num1 / 2) + (num2 / 2) + sign1;
			}
		}
	}

	public static final Number average2(final int num1, final int num2) {
		// Avoid division because it is mean to computers.
		if (num1 == 0) {
			if (num2 >= 0) {
				return (num2 & 1) == 0 ? (num2 >> 1) : num2 * 0.5;
			} else {
				return (num2 & 1) == 0 ? (num2 >> 1) + 1 : num2 * 0.5;
			}
		}

		if (num2 == 0) {
			if (num1 >= 0) {
				return (num1 & 1) == 0 ? (num1 >> 1) : num1 * 0.5;
			} else {
				return (num1 & 1) == 0 ? (num1 >> 1) + 1 : num1 * 0.5;
			}
		}

		final int sign1 = Integer.signum(num1);
		final int sign2 = Integer.signum(num2);

		final boolean differentSign = sign1 != sign2;

		if (differentSign) {
			final int together = num1 + num2;

			return (together & 1) == 0 ? together >> 1 : together * 0.5;
		} else {
			final boolean even1 = (num1 & 1) == 0;

			if (even1 ^ ((num2 & 1) == 0)) { // One is even
				return (num1 >> 1) + (num2 >> 1) - ((sign1 - 1) >> 1) + 0.5 * sign1;
			} else if (even1) { // Both are even
				return (num1 >> 1) + (num2 >> 1);
			} else { // Both are odd
				return (num1 >> 1) + (num2 >> 1) + 1;
			}
		}
	}

	public static final Number average3(final int num1, final int num2) {
		final long together = (long) num1 + num2;

		if ((together & 1) == 0) {
			return (int) (together >> 1);
		} else {
			return together * 0.5D;
		}
	}

	public static final double average0(final int num1, final int num2) {
		return (num1 + num2) * 0.5;
	}

	public static final long pow(final int base, final int exp) {
		if (exp < 0) throw new IllegalArgumentException();
		long result = 1;

		for (int i = 0; i < exp; i++) {
			result *= base;
		}

		return result;
	}

	public static final double pow(final double base, final int exp) {
		if (exp < 0) throw new IllegalArgumentException();
		double result = 1;

		for (int i = 0; i < exp; i++) {
			result *= base;
		}

		return result;
	}

	public static final double powFast(final int base, final int exp) {
		if (exp == 0) return 1.0;
		if (exp < 0) throw new IllegalArgumentException();

		double result = base;

		final int[] factors = factorExp(exp);
		for (final int factor : factors) {
			final double oldResult = result;
			for (int i = 1; i < factor; i++) {
				result *= oldResult;
			}
		}

		return result;
	}

	public static final double powFast(final double base, final int exp) {
		if (exp == 0) return 1.0;
		if (exp < 0) throw new IllegalArgumentException();

		double result = base;

		final int[] factors = factorExp(exp);
		for (final int factor : factors) {
			final double oldResult = result;
			for (int i = 1; i < factor; i++) {
				result *= oldResult;
			}
		}

		return result;
	}

	private static final int[] factorExp(int exp) {
		if (exp < 1) throw new IllegalArgumentException();
		int[] result = new int[(int) Math.sqrt(exp) + 1];
		int idx = 0;

		for (int i = 2; exp >= i; i++) {
			while (exp % i == 0) {
				result[idx] = i;
				exp /= i;
				idx++;
			}
		}

		return Arrays.copyOfRange(result, 0, idx);
	}

	private static final String SONG_LINE1_LAYOUT = "%d bottles of %s on the wall\n";
	private static final String SONG_LINE2_LAYOUT = "%d bottles of %s\n";
	private static final String SONG_LINE3 = "If one of those bottles should happen to fall\n";
	private static final String SONG_LINE4_NORMAL = "There'd be %d bottles of %s on the wall\n";
	private static final String SONG_LINE4_END = "There'd be no more bottles of %s on the wall\n";

	public static final void printBottleSong(final String bottleType) {
		for (int i = 99; i >= 0; i--) {
			int iNow = i + 1;
			System.out.format(SONG_LINE1_LAYOUT, iNow, bottleType);
			System.out.format(SONG_LINE2_LAYOUT, iNow, bottleType);
			System.out.print(SONG_LINE3);

			if (i == 0) {
				System.out.format(SONG_LINE4_END, bottleType);
			} else {
				System.out.format(SONG_LINE4_NORMAL, i, bottleType);
			}
		}
	}

	public static final void printTriangle(int width, final char stamp) {
		for (; width > 0; width--) {
			for (int i = width - 1; i >= 0; i--) {
				System.out.print(stamp);

				if (i > 0) {
					System.out.print(' ');
				}
			}

			System.out.println();
		}
	}

	public static void main(final String[] args) {
		System.out.println("Divisibility by 3:");
		for (int i = 0; i < 10; i++) {
			System.out.format("%d is %s divisible by 3\n", i, evenlyDivisibleBy3(i) ? "indeed" : "not");
		}

		System.out.println("\nAveraging Java int max and Java int max - 50");
		System.out.println(average2(Integer.MAX_VALUE, Integer.MAX_VALUE - 50));

		System.out.println("\nRasing 2 to the power of 4:");
		System.out.println(Math.round(powFast(2, 4)));

		System.out.println("\nBottle song:");
		printBottleSong("pop");

		System.out.println("\nTriangle:");
		printTriangle(3, '#');
	}
}
