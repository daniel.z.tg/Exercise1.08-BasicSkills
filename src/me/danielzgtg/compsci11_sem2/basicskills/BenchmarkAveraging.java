package me.danielzgtg.compsci11_sem2.basicskills;

public final class BenchmarkAveraging {

	public static void main(final String[] ignore) {
		long startTime, endTime;

		System.out.println("Hi");
		Math.abs(0);
		Integer.signum(0);
		bench1();
		bench1();
		bench1();
		bench1();
		bench2();
		bench2();
		bench2();
		bench2();
		bench3();
		bench3();
		bench3();
		bench3();

		startTime = System.nanoTime();
		bench2();
		bench2();
		bench2();
		bench2();
		bench2();
		bench2();
		bench2();
		bench2();
		endTime = System.nanoTime();
		System.out.println("Division: " + (endTime - startTime));

		startTime = System.nanoTime();
		bench1();
		bench1();
		bench1();
		bench1();
		bench1();
		bench1();
		bench1();
		bench1();
		endTime = System.nanoTime();
		System.out.println("BitShift: " + (endTime - startTime));

		startTime = System.nanoTime();
		bench3();
		bench3();
		bench3();
		bench3();
		bench3();
		bench3();
		bench3();
		bench3();
		endTime = System.nanoTime();
		System.out.println("LongShift: " + (endTime - startTime));

		System.out.println("OK");
	}

	private static final void bench1() {
		debugAverage(5, 12);
		debugAverage(-5, -12);
		debugAverage(5, -12);
		debugAverage(-5, 12);
		debugAverage(6, 12);
		debugAverage(-6, -12);
		debugAverage(6, -12);
		debugAverage(-6, 12);
		debugAverage(6, 13);
		debugAverage(-6, -13);
		debugAverage(6, -13);
		debugAverage(-6, 13);
		debugAverage(5, 13);
		debugAverage(-5, -13);
		debugAverage(5, -13);
		debugAverage(-5, 13);
	}

	private static final void bench2() {
		debugAverage2(5, 12);
		debugAverage2(-5, -12);
		debugAverage2(5, -12);
		debugAverage2(-5, 12);
		debugAverage2(6, 12);
		debugAverage2(-6, -12);
		debugAverage2(6, -12);
		debugAverage2(-6, 12);
		debugAverage2(6, 13);
		debugAverage2(-6, -13);
		debugAverage2(6, -13);
		debugAverage2(-6, 13);
		debugAverage2(5, 13);
		debugAverage2(-5, -13);
		debugAverage2(5, -13);
		debugAverage2(-5, 13);
	}

	private static final void bench3() {
		debugAverage3(5, 12);
		debugAverage3(-5, -12);
		debugAverage3(5, -12);
		debugAverage3(-5, 12);
		debugAverage3(6, 12);
		debugAverage3(-6, -12);
		debugAverage3(6, -12);
		debugAverage3(-6, 12);
		debugAverage3(6, 13);
		debugAverage3(-6, -13);
		debugAverage3(6, -13);
		debugAverage3(-6, 13);
		debugAverage3(5, 13);
		debugAverage3(-5, -13);
		debugAverage3(5, -13);
		debugAverage3(-5, 13);
	}

	private static final void debugAverage(final int num1, final int num2) {
		double calculated = BasicSkills.average1(num1, num2).doubleValue();
		double real = (num1 + num2) * 0.5;
		if (Math.abs(calculated - real) < 1) {
			return;
		} else {
			throw new AssertionError(
					String.format("Wrong answer: [%3d, %3d] Real: %2f Actual: %2f", num1, num2, real, calculated));
		}
	}

	private static final void debugAverage2(final int num1, final int num2) {
		double calculated = BasicSkills.average2(num1, num2).doubleValue();
		double real = (num1 + num2) * 0.5;
		if (Math.abs(calculated - real) < 1) {
			return;
		} else {
			throw new AssertionError(
					String.format("Wrong answer: [%3d, %3d] Real: %2f Actual: %2f", num1, num2, real, calculated));
		}
	}


	private static final void debugAverage3(final int num1, final int num2) {
		double calculated = BasicSkills.average3(num1, num2).doubleValue();
		double real = (num1 + num2) * 0.5;
		if (Math.abs(calculated - real) < 1) {
			return;
		} else {
			throw new AssertionError(
					String.format("Wrong answer: [%3d, %3d] Real: %2f Actual: %2f", num1, num2, real, calculated));
		}
	}

	private BenchmarkAveraging() { throw new UnsupportedOperationException(); }
}
