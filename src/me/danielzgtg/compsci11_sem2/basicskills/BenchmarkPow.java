package me.danielzgtg.compsci11_sem2.basicskills;

public final class BenchmarkPow {

	public static void main(final String[] ignore) {
		long startTime, endTime;
		System.out.println("Hi");

		System.out.println(BasicSkills.powFast(2, 712));
		System.out.println(BasicSkills.pow(2, 712));
		checkPowFast(2, 712);
		checkPowFast(2, 712);
		checkPowFast(2, 712);
		checkPowFast(2, 712);
		checkPowFast(2, 712);
		checkPowFast(2, 712);
		checkPowSlow(2, 712);
		checkPowSlow(2, 712);
		checkPowSlow(2, 712);
		checkPowSlow(2, 712);
		checkPowSlow(2, 712);
		checkPowSlow(2, 712);
		System.nanoTime();

		startTime = System.nanoTime();
		checkPowSlow(2, 711);
		endTime = System.nanoTime();
		System.out.println("SimplePow: " + (endTime - startTime));

		startTime = System.nanoTime();
		checkPowFast(2, 711);
		endTime = System.nanoTime();
		System.out.println("FastPow: " + (endTime - startTime));


		System.out.println("OK");
	}
	
	public static void checkPowFast(final int base, final int exp) {
		final double calculated = BasicSkills.powFast(base, exp);
		final double actual = Math.pow(base, exp);
		
		if (Math.abs(calculated - actual) < 1) {
			return;
		} else {
			throw new AssertionError(
					String.format("Wrong answer: [%3d, %3d] Real: %2f Actual: %2f", base, exp, actual, calculated));
		}
	}

	
	public static void checkPowSlow(final int base, final int exp) {
		final double calculated = BasicSkills.pow((double) base, exp);
		final double actual = Math.pow(base, exp);
		
		if (Math.abs(calculated - actual) < 1) {
			return;
		} else {
			throw new AssertionError(
					String.format("Wrong answer: [%3d, %3d] Real: %2f Actual: %2f", base, exp, actual, calculated));
		}
	}

	private BenchmarkPow() { throw new UnsupportedOperationException(); }
}
